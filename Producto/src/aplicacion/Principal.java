package aplicacion;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Principal extends Application{
@Override
    public void start(Stage primaryStage)  {
        
      
        try {
            AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("/vista/InformacionProducto.fxml"));
            Scene scene = new Scene(root);
           // scene.getStylesheets().add(getClass().getResource("cliente.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("Gestión de productos");
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
