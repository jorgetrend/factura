package controlador;

import conexion.Conexion;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import modelo.CFDI;
import modelo.Cliente;
import modelo.Empresa;
import modelo.FormaPago;
import modelo.MetodoPago;
import modelo.TipoPago;
import org.controlsfx.control.textfield.TextFields;

public class FormularioFacturaController implements Initializable {

    @FXML
    private TextField txtCliente;
    @FXML
    private TextField txtFolio;
    @FXML
    private TextField txtFecha;
    @FXML
    private ComboBox<CFDI> cbxUsoCfdi;
    @FXML
    private TextField txtBancoNum;
    @FXML
    private TextField txtTipoCambio;
    @FXML
    private ComboBox<FormaPago> cbxFormaPago;
    @FXML
    private ComboBox<?> cbxMoneda;
    @FXML
    private ComboBox<MetodoPago> cbxMetodoPago;
    @FXML
    private ComboBox<TipoPago> cbxTipo;
    @FXML
    private TextField txtSerie;

    //Listas para ComboBox  
    private ObservableList<Cliente> listaClientes;
    private ObservableList<Empresa> listaEmpresas;
    private ObservableList<FormaPago> listaFormaPago;
    private ObservableList<MetodoPago> listaMetodoPago;
    private ObservableList<TipoPago> listaTipoPago;
    private ObservableList<CFDI> listaCFDI;

    //Conexión
    private Conexion conexion;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conexion = new Conexion();
        conexion.establecerConexion();

        listaFormaPago = FXCollections.observableArrayList();
        listaClientes = FXCollections.observableArrayList();
        listaMetodoPago = FXCollections.observableArrayList();
        listaTipoPago = FXCollections.observableArrayList();
        listaCFDI = FXCollections.observableArrayList();

        FormaPago.llenarDatos(conexion.getConnection(), listaFormaPago);
        MetodoPago.llenarDatos(conexion.getConnection(), listaMetodoPago);
        TipoPago.llenarDatos(conexion.getConnection(), listaTipoPago);
        CFDI.llenarDatos(conexion.getConnection(), listaCFDI);
        Cliente.llenarDatos(conexion.getConnection(), listaClientes);

        cbxFormaPago.setItems(listaFormaPago);
        cbxMetodoPago.setItems(listaMetodoPago);
        cbxTipo.setItems(listaTipoPago);
        cbxUsoCfdi.setItems(listaCFDI);

        TextFields.bindAutoCompletion(txtCliente, listaClientes);

        conexion.cerrarConexion();
    }

}
