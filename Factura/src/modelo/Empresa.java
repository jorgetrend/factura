package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class Empresa {

    //Declaración de variables 
    private IntegerProperty id;
    private StringProperty nombre;

    //Construcción
    public Empresa(Integer id, String nombre) {
        this.id = new SimpleIntegerProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
    }
    //Get & Set

    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    //Método que muestra la lista de datos
    public static void llenarDatos(Connection connection,
            ObservableList<Empresa> listaEmpresas) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(
                    "SELECT id, "
                    + "nombre "
                    + "FROM empresa"
            );
            while (resultado.next()) {
                listaEmpresas.add(
                        new Empresa(
                                resultado.getInt("id"),
                                resultado.getString("nombre"))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
