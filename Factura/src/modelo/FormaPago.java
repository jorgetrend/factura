package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class FormaPago {

    //Declaración de variables
    private IntegerProperty id;
    private StringProperty nombre;

    //Construcción
    public FormaPago(Integer id, String nombre) {
        this.id = new SimpleIntegerProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
    }
    //Get & Set

    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    @Override
    public String toString() {
        return nombre.get();
    }

    //Método que muestra la lista de datos
    public static void llenarDatos(Connection connection,
            ObservableList<FormaPago> lista) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(
                    "SELECT id, "
                    + "nombre "
                    + "FROM formapago"
            );
            while (resultado.next()) {
                lista.add(
                        new FormaPago(
                                resultado.getInt("id"),
                                resultado.getString("nombre"))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
