package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class MetodoPago {

    private IntegerProperty id;
    private StringProperty nombre;

    public MetodoPago(Integer id, String nombre) {
        this.id = new SimpleIntegerProperty(id);
        this.nombre = new SimpleStringProperty(nombre);

    }

    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    @Override
    public String toString() {
        return nombre.get();
    }

    public static void llenarDatos(Connection connection,
            ObservableList<MetodoPago> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT id, "
                    + "nombre "
                    + "FROM metodopago "
            );
            while (resultado.next()) {
                lista.add(
                        new MetodoPago(
                                resultado.getInt("id"),
                                resultado.getString("nombre")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
