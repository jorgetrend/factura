package modelo;

import java.sql.Date;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

public class Factura {

    private IntegerProperty id;
    private IntegerProperty cliente_id;
    private Date fecha;
    private StringProperty observaciones;
    private IntegerProperty subtotal;
    private StringProperty montoI;
    private StringProperty folioFiscal;
    private StringProperty csd;
    private StringProperty condicionesPago;
    private StringProperty formaPago;
    private StringProperty bancoNum;
    private DoubleProperty total;
    private DoubleProperty empresa_id;
    private StringProperty metodoPago;
    private StringProperty emision;
    private StringProperty selloDigital;
    private StringProperty selloSat;
    private StringProperty cadenaOriginal;
    private StringProperty cheque;
    private StringProperty moneda;
    private StringProperty tipo;
    private StringProperty status;
    private StringProperty pagado;
    private StringProperty serie;
    private StringProperty iniciales;
    private StringProperty csdpac;
    private StringProperty tipo2;
    private StringProperty cfdiuso;
    private StringProperty cfdiTipoRelacion;
    private StringProperty cfdiRelacionado;
    private DoubleProperty cfdiTipoCambio;
    private StringProperty cfdiClaveConfirmacion;
    private DoubleProperty cfdiDescuento;
    private DoubleProperty cfdiIVATrasladado;
    private DoubleProperty cfdiIEPSTraslado;
    private DoubleProperty cfdiIVARetenido;
    private DoubleProperty cfdiISRRetenido;
    private StringProperty cfdiVersion;
    private StringProperty rfcEmisorCtaord;
    private StringProperty nomBancoOrdext;
    private StringProperty ctaOrdenante;
    private Date fechaPago;
    private DoubleProperty montoPago;
    private StringProperty monedaPago;
    private StringProperty numPedido;
    private Date fechaEntrega;
    private DoubleProperty valorNeto;
    private StringProperty origenServicio;
    private StringProperty destinoServicio;
    private StringProperty cfdiCondicionesPago;
    private DoubleProperty descuentoMonto;
    private DoubleProperty descuentoPorcentaje;
    private IntegerProperty folio;
    private StringProperty usuario;
    
    

}
