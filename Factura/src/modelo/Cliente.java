package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class Cliente {
    //Declaración de variables
    private IntegerProperty id_cliente;
    private StringProperty nombre_cliente;
    
    //Constructor

    public Cliente(Integer id_cliente, String nombre_cliente) {
        this.id_cliente = new SimpleIntegerProperty(id_cliente);
        this.nombre_cliente = new SimpleStringProperty(nombre_cliente);
    }
    
    //Getters y setters

    public Integer getId_cliente() {
        return id_cliente.get();
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = new SimpleIntegerProperty(id_cliente);
    }

    public String getNombre_cliente() {
        return nombre_cliente.get();
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = new SimpleStringProperty(nombre_cliente);
    }

    @Override
    public String toString() {
        return nombre_cliente.get();
    }
    
    
    
    //Función que muestra la lista de registros de tipo Cliente
    public static void llenarDatos(Connection connection, 
            ObservableList<Cliente> listaClientes){
        
        try {
            Statement statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(
                    "SELECT id, "
                    + "nombre "
                    + "FROM cliente"
            );
            while (resultado.next()) {
                listaClientes.add(
                        new Cliente(
                                resultado.getInt("id"),
                                resultado.getString("nombre"))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
}
