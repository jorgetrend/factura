
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class CFDI {

    //Declaración de variables
    private IntegerProperty id;
    private StringProperty nombre;

    //Constructor
    public CFDI(Integer id, String nombre) {
        this.id = new SimpleIntegerProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
    }

    //Getters y setters
    public Integer getId_cliente() {
        return id.get();
    }

    public void setId_cliente(Integer id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public String getNombre_cliente() {
        return nombre.get();
    }

    public void setNombre_cliente(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    @Override
    public String toString() {
        return nombre.get();
    }

    //Función que muestra la lista de registros de tipo Cliente
    public static void llenarDatos(Connection connection,
            ObservableList<CFDI> lista) {

        try {
            Statement statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(
                    "SELECT id, "
                    + "nombre "
                    + "FROM usocfdi"
            );
            while (resultado.next()) {
                lista.add(
                        new CFDI(
                                resultado.getInt("id"),
                                resultado.getString("nombre"))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
